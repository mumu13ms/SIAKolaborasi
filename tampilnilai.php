<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Landing Page - Start Bootstrap Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Bootstrap basic table example</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="img/LPKIA1.jpg" width="50px" height="50px">
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    
                    <li>
                        <a href="login.php">Login</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <br/><br/><br/><br/><br/><br/>
<div class="container">
	<a href="formnilai.php"><input type="Submit" name="Tambah" value="Tambah" class="btn-primary" /></a>
<br/><br/>
<table class="table">
  <tr>
  	  <th>No</th>	
      <th>ID Matakuliah</th>
      <th>Nama Matakuliah</th>
      <th>Nrp Mahasiswa</th>
      <th>Nama Mahasiswa</th>
      <th>Aksi</th>
  </tr>
  <tr>
      <td>1</td>
      <td>001</td>
      <td>Rekayasa Perangkat Lunak</td>
      <td>6314165</td>
      <td>Mumu Muhammad</td>
      <td>
      	<a href="">Edit</a> ||
      	<a href="">Hapus</a>
      </td>
  </tr>
  <tr>
      <td>2</td>
      <td>002</td>
      <td>Manajemen Proses Bisnis</td>
      <td>6314122</td>
      <td>Akhmad Rivaldi</td>
      <td>
      	<a href="">Edit</a> ||
      	<a href="">Hapus</a>
      </td>
</table>
</div><br /><br />
<footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    
                    <p class="copyright text-muted small">Copyright &copy; Kolaborasi Group 5 and Group 1 2017. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
